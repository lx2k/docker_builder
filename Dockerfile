ARG ARCH=
FROM ${ARCH}ubuntu:latest
ENV XZ_DEFAULTS="-T 0"
ENV DEBIAN_FRONTEND="noninteractive"
RUN apt update;\
apt upgrade -y;\
apt install -y gcc g++ gcc-10 g++-10 cppcheck make flex libelf-dev libssl-dev libudev-dev uuid-dev bison bc dpkg-dev rsync kmod cpio openssl gpg git \
zlib1g-dev git autoconf libtool automake autotools-dev gettext libblkid-dev lsb-release devscripts build-essential dput curl python2 python3 \
python3-dev python3-cffi python3-setuptools python3-pip python3-distutils libdevmapper-dev libattr1-dev libaio-dev libacl1-dev \
gdebi-core gawk alien perl wget acpica-tools device-tree-compiler xz-utils libc6-dev ca-certificates p7zip p7zip-full dosfstools sudo \
e2fsprogs parted mtools u-boot-tools e2tools qemu-system-arm unzip pandoc meson ninja-build libnl-3-dev libnl-genl-3-dev libglib2.0-dev \
ca-certificates;
RUN pip install packaging distlib
